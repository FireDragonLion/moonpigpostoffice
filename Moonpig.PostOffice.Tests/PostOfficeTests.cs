﻿namespace Moonpig.PostOffice.Tests
{
    using Domain;
    using Moonpig.PostOffice.Data;
    using Moonpig.PostOffice.Tests.FakeRepository;
    using Moq;
    using Shouldly;
    using System;
    using System.Collections.Generic;
    using Xunit;

    public class PostOfficeTests
    {
        private Mock<IDbContext> _dbContext;
        private PostOffice _postOffice;
        public PostOfficeTests()
        {
            _dbContext = new Mock<IDbContext>();
            _dbContext.Setup(x => x.Products).Returns(new FakeDbContext().Products);
            _dbContext.Setup(x => x.Suppliers).Returns(new FakeDbContext().Suppliers);
            _postOffice = new PostOffice(_dbContext.Object);
        }

        [Fact]
        public void OneProductWithLeadTimeOfOneDay()
        {
            
            var date = _postOffice.CalculateDespatchDate(new Order(new List<int>() {1}, DateTime.Now));
            date.Date.ShouldBe(DateTime.Now.Date.AddDays(1));
        }

        [Fact]
        public void OneProductWithLeadTimeOfTwoDay()
        {
            var date = _postOffice.CalculateDespatchDate(new Order(new List<int>() { 2 }, DateTime.Now));
            date.Date.ShouldBe(DateTime.Now.Date.AddDays(2));
        }

        [Fact]
        public void OneProductWithLeadTimeOfThreeDay()
        {
            var date = _postOffice.CalculateDespatchDate(new Order(new List<int>() { 3 }, DateTime.Now));
            date.Date.ShouldBe(DateTime.Now.Date.AddDays(3));
        }

        [Fact]
        public void SaturdayHasExtraTwoDays()
        {
            var date = _postOffice.CalculateDespatchDate(new Order(new List<int>() { 1 }, new DateTime(2018,1,27)));
            date.Date.ShouldBe(new DateTime(2018, 1, 27).Date.AddDays(3));
        }

        [Fact]
        public void SundayHasExtraDay()
        {
            var date = _postOffice.CalculateDespatchDate(new Order(new List<int>() { 1 }, new DateTime(2018, 1, 28)));
            date.Date.ShouldBe(new DateTime(2018, 1, 28).Date.AddDays(2));
        }

        [Fact]
        public void When_Order_has_more_than_1_product_and_supplier_CalculateDespatchDate_should_return_max_lead_Time()
        {
            var date = _postOffice.CalculateDespatchDate(new Order(new List<int>() { 1, 2, 3 }, new DateTime(2018, 1, 23)));
            date.Date.ShouldBe(new DateTime(2018, 1, 23).Date.AddDays(3));
        }
        [Fact]
        public void When_Order_has_more_than_1_product_and_supplier_and_order_is_Friday_CalculateDespatchDate_should_return_max_lead_Time_plus_2_more_days()
        {
            var date = _postOffice.CalculateDespatchDate(new Order(new List<int>() { 1, 2, 3 }, new DateTime(2018, 1, 26)));
            date.Date.ShouldBe(new DateTime(2018, 1, 26).Date.AddDays(5));
        }
        [Fact]
        public void When_Order_has_more_than_1_product_and_supplier_and_order_is_Saturday_CalculateDespatchDate_should_return_max_lead_Time_plus_2_more_days()
        {
            var date = _postOffice.CalculateDespatchDate(new Order(new List<int>() { 1, 2, 3 }, new DateTime(2018, 1, 26)));
            date.Date.ShouldBe(new DateTime(2018, 1, 26).Date.AddDays(5));
        }
        [Fact]
        public void When_Order_has_more_than_1_product_and_supplier_and_order_is_Sunday_CalculateDespatchDate_should_return_max_lead_Time_plus_2_more_days()
        {
            var date = _postOffice.CalculateDespatchDate(new Order(new List<int>() { 1, 2, 3 }, new DateTime(2018, 1, 26)));
            date.Date.ShouldBe(new DateTime(2018, 1, 26).Date.AddDays(5));
        }
        [Fact]
        public void When_OrderDate_is_Friday_and_Supplier_lead_time_is_2_days_then_CalculateDespatchDate_should_be_on_Tuesday() {
            var date = _postOffice.CalculateDespatchDate(new Order(new List<int>() { 2 }, new DateTime(2018, 5, 25)));
            date.Date.ShouldBe(new DateTime(2018, 5, 29));
        }
        [Fact]
        public void When_OrderDate_is_Friday_and_Supplier_lead_time_is_3_days_then_CalculateDespatchDate_should_be_on_Wednesday()
        {
            var date = _postOffice.CalculateDespatchDate(new Order(new List<int>() { 3 }, new DateTime(2018, 5, 25)));
            date.Date.ShouldBe(new DateTime(2018, 5, 30));
        }
    }
}
