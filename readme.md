# Moonpig Engineering recruitment test

We've not set a time limit, do whatever you feel is reasonable however consider
 this to be production quality code. 

When complete please upload your solution and answers in a .zip to the google
drive link provided to you by the recruiter.

-----

Parts 1 & 2 have already been completed albeit lacking in quality, please 
check the implementation of these and look at refactoring it into something 
that you consider to be clean and well tested.

Then extend your solution to capture the requirements listed in part 3.

Do not change the public interface `IPostOffice`. The provided DbContext
is a stubbed class which provides test data. Please feel free to use this
in your implementation and tests but do keep in mind that it would be 
switched for something like an EntityFramework DBContext backed by a 
real database in production.

Once you have completed the exercise please answer the questions listed below. 
We are not looking for essay length answers. You can add the answers in this 
document.

## Questions

Q1. What 'code smells' / anti-patterns did you find in the existing 
	implemention of part 1 & 2? 
	
	The code smells I have detected were:
	1. The DbContext class returns IQueryable of Products and Supliers that where hardcoded:
	   perhaps a better implementation for returning these results in a more flexible and decoupled way would be  
       by parsing values from a csv or json file. This way, whenever more or less results where needed, the file could be 
	   easily manipulated without having to change the DbContext class.
	2. SOLID principles weren't taken into consideration on the original written code:
	   -Single Responsibility was not followed on the original CalculateDespatchDate method;
	   -Dependency injection was not implemented and a clear dependency between the data layer and the domain
	    layer was created. This not only makes testing a more difficult process, but also makes the domain layer code 
		maintenance more difficult in case the data layer strucuture changes. 
	   -Lack of repositories and data logic applied directly on the domain layer:
	    all the data logic should be decoupled from the domain layer and data layer specific methods 
		should be tested agnostically.
		
Q2. What further steps would you take to improve the solution given more time?
    
	I would add IRepository with common repositoy method signatures, IProductRepository and ISupplierRepository to contain 
	specific methods regarding Product and Supplier logic respectively. I would then create ProductRepository and
	SupplierRepository classes that would implement IRepository and IProductRepository or ISupplierRepository.
	I would inject DbContext onto ProductRepository and SupplierRepository through IDbContext. 
	I would move all the GetSupplierLeadDateTime, GetSuppliersByProductIds and GetSupplierIdsByProductIds methods 
	into the Supplier repository as public methods and test them agnostically in a new PostOffice.Tests project.
	I would inject ProductRepository and SupplierRepository into PostOffice.cs through their interfaces,
	and remove the DbContext injection.
	
Q3. What's a technology that you're excited about and where do you see this 
    being applicable?
	
	I am excited about trying GraphQL, I would see the domain logical layer from the exercise being used to
	handle the graph payload requests. 

Q4. What process would you take to identify a performance issue in a production
    environment? 
	
	If ms sql server related issue, would use sql profiler to analyse sql queries;
	if code related the would use a third party tool like New Relic, Raygun, or Dynatrace;

## Programming Exercise - Moonpig Post Office

You have been tasked with creating a service that calculates the estimated 
despatch dates of customers' orders. 

An order contains a collection of products that a customer has added to their 
shopping basket. 

Each of these products is supplied to Moonpig on demand through a number of 
3rd party suppliers.

As soon as an order is received by a supplier, the supplier will start 
processing the order. The supplier has an agreed lead time in which to 
process the order before delivering it to the Moonpig Post Office.

Once the Moonpig Post Office has received all products in an order it is 
despatched to the customer.  

Assumptions:

1. Suppliers start processing an order on the same day that the order is 
	received. For example, a supplier with a lead time of one day, receiving
	an order today will send it to Moonpig tomorrow.


2. For the purposes of this exercise we are ignoring time i.e. if a 
	supplier has a lead time of 1 day then an order received any time on 
	Tuesday would arrive at Moonpig on the Wednesday.

3. Once all products for an order have arrived at Moonpig from the suppliers, 
	they will be despatched to the customer on the same day.


### Part 1 

Write an implementation of `IPostOffice` that calculates the despatch date 
of an order. 


### Part 2

Moonpig Post Office staff are getting complaints from customers expecting 
packages to be delivered on the weekend. You find out that the Moonpig post
office is shut over the weekend. Packages received from a supplier on a weekend 
will be despatched the following Monday.

Modify the existing code to ensure that any orders received from a supplier
on the weekend are despatched on the following Monday.

### Part 3

The Moonpig post office is still getting complaints... It turns out suppliers 
don't work during the weekend as well, i.e. if an order is received on the 
Friday with a lead time of 2 days, Moonpig would receive and dispatch on the 
Tuesday.


Modify the existing code to ensure that any orders that would have been 
processed during the weekend resume processing on Monday.
