﻿namespace Moonpig.PostOffice.Domain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Data;

    public class PostOffice : IPostOffice
    {
        private DateTime _mlt;
        private readonly IDbContext _dbContext;

        public PostOffice(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public DateTime CalculateDespatchDate(Order order)
        {
            var suppliersInOrder = GetSuppliersByProductIds(order.ProductIds);

            _mlt = GetSupplierLeadDateTime(suppliersInOrder, order.OrderDate);

            if (order.OrderDate.DayOfWeek.Equals(DayOfWeek.Saturday))
            {
                return _mlt.AddDays(2);
            }
            else if (order.OrderDate.DayOfWeek.Equals(DayOfWeek.Sunday))
            {
                return _mlt.AddDays(1);
            }
            else return _mlt;
        }

        private DateTime GetSupplierLeadDateTime(IEnumerable<Supplier> suppliers, DateTime orderDate)
        {

            var maxLeadTime = suppliers.Max(x => x.LeadTime);

            if (!orderDate.DayOfWeek.Equals(DayOfWeek.Saturday) && !orderDate.DayOfWeek.Equals(DayOfWeek.Sunday))
            {
                if (orderDate.AddDays(maxLeadTime - 1).DayOfWeek.Equals(DayOfWeek.Saturday)
                    || orderDate.AddDays(maxLeadTime - 1).DayOfWeek.Equals(DayOfWeek.Sunday))
                {
                    maxLeadTime = maxLeadTime + 2;
                }
            }
            var result = orderDate.AddDays(maxLeadTime);
            return result;
        }

        private IEnumerable<Supplier> GetSuppliersByProductIds(IEnumerable<int> productIds)
        {
            var supplierIdsInOrder = GetSupplierIdsByProductIds(productIds);
            return _dbContext.Suppliers.Where(x => supplierIdsInOrder.Contains(x.SupplierId));
        }

        private IEnumerable<int> GetSupplierIdsByProductIds(IEnumerable<int> productIds)
        {
            return _dbContext.Products.Where(x => productIds.Contains(x.ProductId)).Distinct().Select(x => x.SupplierId);
        }
    }
}